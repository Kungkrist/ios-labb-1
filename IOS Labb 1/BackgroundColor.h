//
//  BackgroundColor.h
//  IOS Labb 1
//
//  Created by DEA on 2016-01-30.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface BackgroundColor : NSObject

+ (void) setColor : (UIColor *) value;
+ (UIColor *) getColor;

@end

static UIColor *rgb;