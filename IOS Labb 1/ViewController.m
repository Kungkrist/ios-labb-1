//
//  ViewController.m
//  IOS Labb 1
//
//  Created by DEA on 2016-01-30.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "ViewController.h"
#import "BackgroundColor.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *mainView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void) viewWillAppear:(BOOL)animated{
    if([BackgroundColor getColor] != nil)
        self.mainView.backgroundColor = [BackgroundColor getColor];

}


@end
