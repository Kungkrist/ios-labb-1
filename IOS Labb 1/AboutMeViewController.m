//
//  AboutMeViewController.m
//  IOS Labb 1
//
//  Created by DEA on 2016-01-30.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "AboutMeViewController.h"
#import "BackgroundColor.h"

@interface AboutMeViewController ()
@property (strong, nonatomic) IBOutlet UIView *aboutMeView;

@end

@implementation AboutMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    if([BackgroundColor getColor] != nil)
        self.aboutMeView.backgroundColor = [BackgroundColor getColor];
    
}


@end
