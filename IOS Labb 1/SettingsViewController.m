//
//  SettingsViewController.m
//  IOS Labb 1
//
//  Created by DEA on 2016-01-30.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "SettingsViewController.h"
#import "BackgroundColor.h"

@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (strong, nonatomic) IBOutlet UIView *settingsView;

@end

@implementation SettingsViewController

BackgroundColor *backgroundColorInstance;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    backgroundColorInstance = [[BackgroundColor alloc] init];
    
    if([BackgroundColor getColor] != nil)
        self.settingsView.backgroundColor = [BackgroundColor getColor];
    
}

- (IBAction)changeColor:(UISlider *)sender {
    [BackgroundColor setColor:[self mixedColor]];
    self.settingsView.backgroundColor = [BackgroundColor getColor];
}

- (UIColor *) mixedColor{
    return [UIColor colorWithRed:(self.redSlider.value) green:(self.greenSlider.value) blue:(self.blueSlider.value) alpha:(1)];
}
@end
