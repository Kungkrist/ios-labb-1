//
//  BackgroundColor.m
//  IOS Labb 1
//
//  Created by DEA on 2016-01-30.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "BackgroundColor.h"

@implementation BackgroundColor

+ (void)setColor:(UIColor *)value{
    rgb = value;
}

+ (UIColor *)getColor{
    return rgb;
}
@end
